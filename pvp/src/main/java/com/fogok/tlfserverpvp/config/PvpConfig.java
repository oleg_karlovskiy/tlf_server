package com.fogok.tlfserverpvp.config;

import com.fogok.tlfserver.config.BaseConfigModel;

public class PvpConfig extends BaseConfigModel {

    @Override
    public void createDefaultConfigModel() {
        params.put("port", "15504");
    }

}
