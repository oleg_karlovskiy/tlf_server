package com.fogok.tlfserver;

import com.fogok.tlfserver.config.BaseConfigModel;
import io.netty.channel.ChannelInboundHandlerAdapter;

public abstract class BaseTcpChannelInboundHandlerAdapter<T extends BaseConfigModel> extends ChannelInboundHandlerAdapter {

    protected T config;

    public void init(T config){
        setConfig(config);
        init();
    }

    public abstract void init();

    public void setConfig(T config) {
        this.config = config;
    }

    public T getConfig() {
        return config;
    }
}
