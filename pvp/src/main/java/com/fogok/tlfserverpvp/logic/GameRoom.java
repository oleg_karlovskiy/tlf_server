package com.fogok.tlfserverpvp.logic;

import com.fogok.dataobjects.utils.libgdxexternals.Pool;

import java.util.HashMap;
import java.util.HashSet;

import static com.esotericsoftware.minlog.Log.info;
import static com.esotericsoftware.minlog.Log.trace;

public class GameRoom implements Pool.Poolable{

    public static final int PLAYERS_MAX = 2;

    public enum LobbyRoomState {
        WaitingPlayers,
        Playing,
        InPause
    }

    private LobbyRoomState lobbyRoomState = LobbyRoomState.WaitingPlayers;

    private HashMap<String, Player> connectedPlayers = new HashMap<>(PLAYERS_MAX);
    private HashSet<String> waitingPlayersToConnect = new HashSet<>(PLAYERS_MAX);

    public volatile int roomId;
    public volatile String roomName;

    public synchronized void connectPlayer(Player player) {
        if (connectedPlayers.size() == PLAYERS_MAX)
            throw new IllegalStateException("connectedPlayers.size == PLAYERS_MAX");

        connectedPlayers.put(player.id, player);

        waitingPlayersToConnect.remove(player.id);
    }

    public synchronized void disconnectPlayer(Player player) {
        connectedPlayers.remove(player.id);
        if (waitingPlayersToConnect.contains(player.id))
            throw new IllegalStateException(String.format("What??? Same player id(%s) in waitingPlayersToConnect", player.id));
        waitingPlayersToConnect.add(player.id);
    }

    public synchronized void process(){
//        trace(String.format("processing room with \"%s\" id. Players in room: \"%s\"", roomId, connectedPlayers.values().stream().map( n -> n.id )
//                .collect( Collectors.joining( "," ))));
    }

    public synchronized void finish(){

    }

    public synchronized boolean isFilled(){
        return connectedPlayers.size() == PLAYERS_MAX;
    }

    public synchronized boolean isEmpty() {
        return connectedPlayers.size() == 0;
    }

    public synchronized HashSet<String> getWaitingPlayersToConnect() {
        return waitingPlayersToConnect;
    }

    @Override
    public synchronized void reset() {
        connectedPlayers.clear();
        lobbyRoomState = LobbyRoomState.WaitingPlayers;
    }
}
