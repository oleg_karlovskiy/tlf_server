package com.fogok.tlfserverpvp.logic;

import com.esotericsoftware.kryo.io.ByteBufferInput;
import com.fogok.dataobjects.ConnectToServiceImpl;
import com.fogok.dataobjects.header.FirstPackage;
import com.fogok.dataobjects.header.PackageType;
import com.fogok.dataobjects.utils.Serialization;
import com.fogok.dataobjects.utils.libgdxexternals.Array;
import com.fogok.dataobjects.utils.libgdxexternals.Pool;
import com.fogok.tlfserver.utlis.ExecutorToThreadPool;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.util.ReferenceCountUtil;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.esotericsoftware.minlog.Log.*;

public enum GmRoomManager {
    instance;

    private final int MAX_ROOMS_COUNT = 150;
    private final int ROOM_FPS = 30;

    private final Array<AtomicBoolean> roomThreadStopTriggers = new Array<>(MAX_ROOMS_COUNT);
    private final ExecutorToThreadPool threadPool = new ExecutorToThreadPool();

    private final DummyMatchmaker dummyMatchmaker = new DummyMatchmaker();

    public final IOActionPool actPool = new IOActionPool();

    private Queue<IOAction> roomLifo = new LinkedList<>(); //TODO: check this!

    private final HashMap<Integer, GameRoom> registeredRooms = new HashMap<>(MAX_ROOMS_COUNT);
    private final HashMap<String, Player> registeredPlayers = new HashMap<>(GameRoom.PLAYERS_MAX * MAX_ROOMS_COUNT);
               //k: player.id, v:roomId
    private HashMap<String, Integer> waitingPlayersWithRoomMap = new HashMap<>(GameRoom.PLAYERS_MAX * MAX_ROOMS_COUNT);

    private final Object gmRoomManagingSync = new Object();
    private final Object regPlayersManagingSync = new Object();
    private final Object roomLifoActionsManageSync = new Object();



    private Stack<Integer> roomIds = new Stack<>();

    /*ctor*/ {
        info("Init GameRoomManager");
        for (int i = 0; i < MAX_ROOMS_COUNT; i++) {
            roomThreadStopTriggers.add(new AtomicBoolean(false));
            roomIds.push(MAX_ROOMS_COUNT - i);
        }
    }

    private void ProcessIOAction(IOAction action) {
        PackageType type = action.packageType;
        switch (type) {
            case Header:
                FirstPackage firstP;
                try {
                    firstP = new FirstPackage().read(action.input);
                } catch (Exception e) {
                    warn(String.format("Exception in deserialize from %s: %s", action.ch.remoteAddress(), e));
                    break;
                }

                initializePlayer(action.ch.remoteAddress().toString(), firstP.clientId);

                break;
            case EVP:
            default:
                throw new IllegalStateException(String.format("Bad package type: %s", type));
        }
        actPool.freeSync(action);
    }

    public void process(ByteBuf buf, Channel ch){
        final IOAction ioAction = actPool.obtainSync(buf, ch);
        if (!ioAction.toRoomProcessor){
            threadPool.execute(() -> ProcessIOAction(ioAction));
        } else {
            synchronized (roomLifoActionsManageSync) {
                roomLifo.add(ioAction);
            }
        }
    }

    private void initializePlayer(String addr, String id){
        synchronized (gmRoomManagingSync) {
            if (!registeredPlayers.containsKey(addr))
                throw new IllegalStateException(String.format("%s is not registered !!!", addr));

            Player player = registeredPlayers.get(addr);
            player.id = id;

            final boolean isReconnect = waitingPlayersWithRoomMap.containsKey(player.id);
            GameRoom roomToConnect;
            if (isReconnect) {
                final int roomId = waitingPlayersWithRoomMap.remove(player.id);
                if (!registeredRooms.containsKey(roomId))
                    throw new IllegalStateException(String.format("%s is not registered in registeredRooms???", roomId));
                roomToConnect = registeredRooms.get(roomId);
            } else {

                //creating new player room
                if (dummyMatchmaker.roomToFill == null || dummyMatchmaker.roomToFill.isFilled()) {
                    GameRoom gameRoom = new GameRoom();
                    gameRoom.roomId = roomIds.pop();
                    gameRoom.roomName = "GameRoom #" + gameRoom.roomId;
                    dummyMatchmaker.roomToFill = gameRoom;
                    if (registeredRooms.containsKey(gameRoom.roomId))
                        throw new IllegalStateException(String.format("%s is registered in registeredRooms???", gameRoom.roomName));
                    registeredRooms.put(gameRoom.roomId, gameRoom);
                    info("Starting " + gameRoom.roomName);
                    threadPool.execute(() -> startProcessingRoomThread(gameRoom));
                }

                roomToConnect = dummyMatchmaker.roomToFill;

            }

            roomToConnect.connectPlayer(player);
            player.room = roomToConnect;
            player.playerState = Player.PlayerState.InRoom;

            info(String.format("Player %s " + (isReconnect ? "reconnected" : "connected") + " to %s", id, player.room.roomName));
        }
    }

    public void connectPlayer(String inetaddr) {
        synchronized (regPlayersManagingSync) {
            if (registeredPlayers.containsKey(inetaddr))
                throw new IllegalStateException(String.format("WHAT? same ip ? %s", inetaddr));
            registeredPlayers.put(inetaddr, new Player(inetaddr));
        }
        info(String.format("Player %s joined to pvp service", inetaddr));
    }

    public void disconnectPlayer(String inetaddr){
        Player playerToDisconnect;
        synchronized (regPlayersManagingSync) {
            playerToDisconnect = registeredPlayers.remove(inetaddr);
        }

        synchronized (gmRoomManagingSync) {
            if (playerToDisconnect.playerState == Player.PlayerState.InVoid) {
                info(String.format("Player %s left from pvp service", playerToDisconnect.inetaddr));
                return;
            }

            GameRoom disconnectedPlayerRoom = playerToDisconnect.room;
            disconnectedPlayerRoom.disconnectPlayer(playerToDisconnect);
            final String disconnectedPlayerRoomName = disconnectedPlayerRoom.roomName;
            if (disconnectedPlayerRoom.isEmpty()) {
                roomThreadStopTriggers.get(disconnectedPlayerRoom.roomId).set(true);

                //clearing roomToFill
                if (dummyMatchmaker.roomToFill != null)
                    if (disconnectedPlayerRoom.roomId == dummyMatchmaker.roomToFill.roomId)
                        dummyMatchmaker.roomToFill = null;

            } else {
                if (waitingPlayersWithRoomMap.containsKey(playerToDisconnect.id))
                    throw new IllegalStateException(String.format("WHAT? same id in waitingPlayersWithRoomMap? %s", playerToDisconnect.id));
                waitingPlayersWithRoomMap.put(playerToDisconnect.id, disconnectedPlayerRoom.roomId);
            }

            info(String.format("Player %s left from pvp service. Removed from %s", inetaddr, disconnectedPlayerRoomName));
        }
    }

    //region Act pool

    /**
     * Action from client
     */
    public static class IOAction implements Pool.Poolable{

        volatile ByteBuf byteBuf;
        volatile Channel ch;
        volatile PackageType packageType;
        volatile boolean toRoomProcessor;

        final ByteBufferInput input = new ByteBufferInput(ByteBuffer.allocate(ConnectToServiceImpl.BUFFER_SIZE));

        //final ByteBufferOutput output = new ByteBufferOutput(ByteBuffer.allocateDirect(ConnectToServiceImpl.BUFFER_SIZE));

        @Override
        public void reset() {

        }

        @Override
        public String toString() {
            return ch.remoteAddress().toString();
        }
    }

    /**
     * Thread safe actions from clients pool impl
     */
    public static class IOActionPool extends Pool<IOAction> {
        @Override
        protected IOAction newObject() {
            return new IOAction();
        }


        synchronized IOAction obtainSync(ByteBuf byteBuf, Channel ch) {
            IOAction ioAction = super.obtain();
            ioAction.ch = ch;
            ioAction.byteBuf = byteBuf;
            ioAction.input.setBuffer(byteBuf.nioBuffer());
            ioAction.packageType = PackageType.Read(ioAction.input);
            ioAction.toRoomProcessor = ioAction.packageType != PackageType.Header;
            return ioAction;
        }

        synchronized void freeSync(IOAction act) {
            ReferenceCountUtil.release(act.byteBuf);
            //act.input.release(); maybe ?
            super.free(act);
        }

        public void poolStatus(){
            info(String.format("Free objects: %s", getFree()));
        }
    }


    //endregion

    private void startProcessingRoomThread(GameRoom gameRoom) {
        final int id = gameRoom.roomId;

        if (id >= MAX_ROOMS_COUNT) {
            error(String.format("MAX_ROOMS_COUNT(%s) is reached", MAX_ROOMS_COUNT));
            return;
        }

        roomThreadStopTriggers.get(id).set(false);
        while (!roomThreadStopTriggers.get(id).get()) {
            //trace("Processing: " + name);

            gameRoom.process();

            try {
                Thread.sleep((int)(1000f / ROOM_FPS));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (!gameRoom.isEmpty()) {
            error("!gameRoom.isEmpty()");
        }

        gameRoom.finish();
        finalizeRoom(gameRoom);
    }

    private void finalizeRoom(GameRoom gameRoom){
        freeRoomId(gameRoom.roomId);

        info(gameRoom.roomName + " is finished");
        info(String.format("Rooms is available: %s", roomIds.size()));
    }

    private void freeRoomId(int id){
        synchronized (gmRoomManagingSync) {
            roomIds.push(id);
            if (!registeredRooms.containsKey(id))
                throw new IllegalStateException(String.format("registeredRooms is not contains key with %s id", id));
            registeredRooms.remove(id);
        }
    }

    public void gracefullyShutDown(){
        for (int i = 0; i < MAX_ROOMS_COUNT; i++)
            roomThreadStopTriggers.get(i).set(true);
        threadPool.awaitTerminate();
    }

}
