package com.fogok.tlfserverpvp.nettyhandlers;

import com.fogok.tlfserver.BaseTcpChannelInboundHandlerAdapter;
import com.fogok.tlfserverpvp.config.PvpConfig;
import com.fogok.tlfserverpvp.logic.GmRoomManager;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import static com.esotericsoftware.minlog.Log.info;

public class PvpHandler extends BaseTcpChannelInboundHandlerAdapter<PvpConfig> {

    @Override
    public void init() {

    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        String id = ctx.channel().remoteAddress().toString();

        GmRoomManager.instance.connectPlayer(id);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        String id = ctx.channel().remoteAddress().toString();

        GmRoomManager.instance.disconnectPlayer(id);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        GmRoomManager.instance.process(((ByteBuf)msg).retain(), ctx.channel());
    }
}
