package com.fogok.tlfserverpvp.config;


import com.fogok.tlfserver.config.BaseConfigReader;
import com.fogok.tlfserver.utlis.CLIArgs;

import java.io.IOException;

public class PvpConfigReader extends BaseConfigReader<PvpConfig> {

    public PvpConfigReader(CLIArgs cliArgs) throws IOException, IllegalAccessException, InstantiationException {
        super(PvpConfig.class, cliArgs.configPath, cliArgs.serviceName);
    }

}
