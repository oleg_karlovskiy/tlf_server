package com.fogok.tlfserver.utlis;

import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.info;

public class InterruptingManager {
    public static void WaitInterrupt(Thread thread, int secs) {
        int count = 0, maxCount = secs;
        while (thread.isAlive()) {
            info(String.format("Waiting for interrupting \"%s\"... %s", thread.getName(), count++));
            if (count > maxCount){
                error(String.format("Don`t interrupted \"%s\"", thread.getName()));
                return;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        info(String.format("Success interrupted \"%s\"", thread.getName()));
    }
}
