package com.fogok.tlfserver.config;

import com.fogok.tlfserver.utlis.CLIArgs;

import java.io.IOException;

public class CommonConfigReader extends BaseConfigReader<CommonConfig>{

    public CommonConfigReader(CLIArgs cliArgs) throws IOException, IllegalAccessException, InstantiationException {
        super(CommonConfig.class, cliArgs.configPath, CommonConfig.class.getSimpleName().toLowerCase());
    }

}
