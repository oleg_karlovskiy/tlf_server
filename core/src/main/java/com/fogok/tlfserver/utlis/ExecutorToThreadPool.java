package com.fogok.tlfserver.utlis;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.esotericsoftware.minlog.Log.*;

public class ExecutorToThreadPool {

    private ExecutorService service = Executors.newCachedThreadPool();

    public void execute(Runnable runnable) {
        service.submit(runnable);
    }

    public void awaitTerminate(){
        try {
            debug("Started termination all executors. 10 seconds timeout");
            service.shutdown();
            boolean terminationSuccess = service.awaitTermination(10, TimeUnit.SECONDS);
            if (terminationSuccess) {
                info("Success termination services!");
            } else {
                error("Bad termination services :( Info: " + service.toString());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            error("Bad termination services :(");
        }
    }

}
