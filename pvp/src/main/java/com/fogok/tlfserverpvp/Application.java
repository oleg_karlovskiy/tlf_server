package com.fogok.tlfserverpvp;

import com.fogok.dataobjects.utils.Serialization;
import com.fogok.tlfserver.baseservice.SimpleExceptionHandler;
import com.fogok.tlfserver.config.BaseConfigModel;
import com.fogok.tlfserver.config.CommonConfigReader;
import com.fogok.tlfserver.database.DBException;
import com.fogok.tlfserver.utlis.CLIArgs;
import com.fogok.tlfserver.utlis.InterruptingManager;
import com.fogok.tlfserver.utlis.ServiceStarter;
import com.fogok.tlfserverpvp.config.PvpConfigReader;
import com.fogok.tlfserverpvp.logic.GmRoomManager;
import com.fogok.tlfserverpvp.nettyhandlers.PvpHandler;
import io.netty.channel.ChannelFuture;

import java.io.IOException;

import static com.esotericsoftware.minlog.Log.*;

public class Application {

    //region Singleton realization
    private static Application instance;
    public static Application getInstance() {
        return instance == null ? instance = new Application() : instance;
    }
    //endregion

    private CLIArgs cliArgs;
    private ChannelFuture mainNettyAccessor;


//    private ConnectorToMongo connectorToMongo; //maybe will connect to mongo, so do not delete comments

    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException, InterruptedException, DBException {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {

            Thread thread = getInstance().getMainThread();
            if (thread != null) {
                thread.interrupt();

                InterruptingManager.WaitInterrupt(thread, 10);
            }else
                error("Main thread is null... ???");


            GmRoomManager.instance.gracefullyShutDown();

        }, "ShutdownHookThread"));

        getInstance().startPvpService(args);
    }

    private Thread mainThread;
    private Thread getMainThread(){
        return mainThread;
    }

    private void startPvpService(String[] args) throws IOException, IllegalAccessException, InstantiationException, InterruptedException, DBException {
        setCliArgs(args);
        BaseConfigModel commonConfig = new CommonConfigReader(cliArgs).getConfig();
//        connectToMongo(commonConfig);
        startServiceForAllClients(commonConfig);
    }

    private void setCliArgs(String[] args) throws IOException {
        cliArgs = ServiceStarter.getInstance().readCLI(args);
        ServiceStarter.getInstance().createLog(cliArgs);
    }

//    private void connectToMongo(BaseConfigModel commonConfig) throws DBException {
//        connectorToMongo = new ConnectorToMongo((CommonConfig) commonConfig);
//    }

    private void startServiceForAllClients(BaseConfigModel commonConfig) throws IOException, InstantiationException, IllegalAccessException, InterruptedException {
        //activate all requered instances
        Serialization.instance.name();
        GmRoomManager.instance.name();
        mainThread = Thread.currentThread();
//        BaseConfigModel qwe = new PvpConfigReader(cliArgs).getConfig().setCommonConfig(commonConfig);
//        System.out.println(qwe.getParams().get("port"));

        //start listener to tcp connects
        ServiceStarter.getInstance().startAndSyncService(new ServiceStarter.ServiceParamsBuilder<SimpleExceptionHandler>()
                .setConfigModel(new PvpConfigReader(cliArgs).getConfig().setCommonConfig(commonConfig))
                .setCliArgs(cliArgs)
                .setCoreTcpHandler(PvpHandler.class)
                .setExceptionHandler(SimpleExceptionHandler.class));
    }


}
