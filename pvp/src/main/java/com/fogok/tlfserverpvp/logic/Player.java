package com.fogok.tlfserverpvp.logic;

public class Player {

    public Player(String inetaddr) {
        this.inetaddr = inetaddr;
    }

//    private enum PlayerState {
//        Lobby_Auth,
//        Lobby_Idle,
//        Lobby_SearchingGame,
//        Lobby_ConnectingToGame,
//
//        Game_WaitingForOtherPlayers,
//        Game_Playing,
//        Game_Pause
//    }
//
//    public PlayerState playerState;


    enum PlayerState{
        InVoid,
        InRoom
    }

    public PlayerState playerState = PlayerState.InVoid;

    public String id;
    public String inetaddr;
    public GameRoom room;


}
